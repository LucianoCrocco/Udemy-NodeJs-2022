//GENERAR ESTA FUNCION PARA DESPEJAR LA BASE DE DATOS Y LA CARPETA IMAGES CADA X TIEMPO.
const Order = require("../models/order");
const Product = require("../models/product");
const User = require("../models/user");
const rootDir = require("./path");
const mongoose = require("mongoose");
const fsExtra = require("fs-extra");
const path = require("path");

const directoryImages = path.join(rootDir, "images");
const directoryInvoices = path.join(rootDir, "data", "invoices");

async function clearAll() {
    await Order.collection.deleteMany({});
    await Product.collection.deleteMany({});
    await User.collection.deleteMany({});

    fsExtra.emptyDirSync(directoryImages);
    fsExtra.emptyDirSync(directoryInvoices);
}

module.exports = clearAll;
