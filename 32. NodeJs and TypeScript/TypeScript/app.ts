const num1Element = document.getElementById("num1") as HTMLInputElement;
const num2Element = document.getElementById("num2") as HTMLInputElement;
const buttonElement = document.querySelector("button");

const numResults: Array<number> = [];
const textResult: string[] = [];

type numOrString = string | number;
type Result = { val: number; timestamp: Date };
interface ResultObj {
    val: number;
    timestamp: Date;
}

function add(num1: number | string, num2: number | string) {
    if (typeof num1 === "number" && typeof num2 === "number") {
        return num1 + num2;
    } else if (typeof num1 === "string" && typeof num2 === "string") {
        return num1 + " " + num2;
    } else {
        return +num1 + +num2;
    }
}

function printResult(resultObj: { val: number; timestamp: Date }) {
    console.log(resultObj.val);
}

console.log(add(1, 2));

buttonElement?.addEventListener("click", () => {
    // -> Se agrega el ? ya que si no se encuentra un boton esto puede ser nulo.
    const num1 = num1Element.value;
    const num2 = num2Element.value;
    const result = add(+num1, +num2);
    numResults.push(result as number);
    const stringResult = add(num1, num2);
    textResult.push(stringResult as string);
    console.log(result);
    console.log(stringResult);
    printResult({ val: result as number, timestamp: new Date() });
    console.log(numResults, textResult);
});
