const {expect} = require("chai");

describe('First tests examples', () => {
    it("should add numbers correctly", () => {
        //Arrange
        const num1 = 2;
        const num2 = 3;
        //Act
        const sum = num1 + num2;
        //Assert
        expect(sum).to.equal(5);
    });
    
    it("should not give a result of 6", () => {
        //Arrange
        const num1 = 2;
        const num2 = 3;
        //Act
        const sum = num1 + num2;
        //Assert
        expect(sum).not.to.equal(6);
    });
})
