const {expect} = require("chai");
const sinon = require("sinon");
const User = require("../models/user");
const AuthController = require("../controller/auth");

describe('Auth controller - Login', () => {
    it("should throw an error with code 500 if accessing the database fails", (done) => {
        sinon.stub(User, "findOne");
        User.findOne.throws(); // -> Fuerza el throw del error cuando se ejecute la funcion

        const req = {
            body : {
                email: "test@test.com",
                password: "tester"
            }
        }

        AuthController.login(req, {}, () => {}).then(result => {
            expect(result).to.be.an("error");
            expect(result).to.have.property("statusCode", 500);
            
        }).catch(err => {
            done(err); // -> According to the Mocha documentation we need to catch the errors in an async call and then pass it to the done function otherwise there will be a timeout error when assertion fails. The code should look like following.
        })
        
        User.findOne.restore();
    })

});