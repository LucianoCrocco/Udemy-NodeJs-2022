const {expect} = require("chai");
const authMiddleware = require("../middleware/is-auth");
const jwt = require("jsonwebtoken");
const sinon = require("sinon");

describe('Auth Middleware', () => { 
    it("should throw an error if no authorization header is present", () => {
        const req = {
            get: function (){
                return null;
            }
        };
        expect(authMiddleware.bind(this, req)).to.throw("Not autheticated");
    })
    
    it("should an error if the authorization header is only one string", () => {
        const req = {
            get: function (){
                return 'xyz';
            }
        };
        expect(authMiddleware.bind(this, req)).to.throw();
    })

    it("should yield a userId after decoding the token", () => {
        const req = {
            get: function (){
                return 'Bearer xyz';
            }
        };
        sinon.stub(jwt, 'verify');
        jwt.verify.returns({userId : 'abc'})
        // jwt.verify = (() => { La manera vieja sin sinon
        //     return {userId : 'abc'};
        // })
        authMiddleware(req, {}, () => {});
        expect(req).to.have.property('userId');
        expect(req).to.have.property('userId', 'abc');
        expect(jwt.verify.called).to.be.true; // Puedo verificar que se haya llamado la funcion gracias a stub
        jwt.verify.restore();
    })
})


